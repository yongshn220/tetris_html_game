class Storage{
    names;
    scores;
    constructor(){
        this.names = [];
        this.scores = [];
        this.backupRanks();
    }

    backupRanks(){
        this.names = [];
        this.scores = [];
        var num = 0;
        var tmpS;
        var tmpN;
        for(num; num < 12; num++){
            tmpS = localStorage.getItem("menuA" + num + "s") * 1;
            tmpN = localStorage.getItem("menuA" + num + "n");
            if(tmpS !== null && tmpN !== null){
                this.scores.push(tmpS);
                this.names.push(tmpN);
            }
        }
        this.showRanks();
    }
    addRank(name, score){
        if(name === null){
            name = "NaN";
        }
        this.names.push(name);
        this.scores.push(score);
        this.saveRanks();
    }
    orderingRanks(){
        var tmpS;
        var tmpN;
        for(var i = 0; i < this.scores.length - 1; i++){
            for(var j = i; j >= 0; j--){
                if(this.scores[j] < this.scores[j + 1]){
                    tmpS = this.scores[j];
                    this.scores[j] = this.scores[j + 1];
                    this.scores[j + 1] = tmpS;

                    tmpN = this.names[j];
                    this.names[j] = this.names[j + 1];
                    this.names[j + 1] = tmpN;
                }
            }
        }
    }
    showRanks(){
        this.orderingRanks();
        var pos = document.querySelector("#r-pos ol");
        var score = document.querySelector("#r-score ol");
        var name = document.querySelector("#r-name ol");
        pos.innerHTML = "";
        score.innerHTML = "";
        name.innerHTML = "";
        for(var i = 0; i < this.scores.length; i++){
            pos.innerHTML += '<li>'+ (i+1) +'</div>';
            score.innerHTML += '<li>'+ this.scores[i] + '</div>';
            name.innerHTML += '<li>'+ this.names[i] + '</div>';
        }
    }

    space(num){
        var str = "";
        for(var i = 0; i < num; i++){
            str += "&nbsp;";
        }
        return str;
    }
    saveRanks(){
        this.localStorageClearThis();
        this.orderingRanks();
        for(var i = 0; i < this.scores.length; i++){
            if(this.scores[i] !== null){
                localStorage.setItem("menuA" + i + "s", this.scores[i] * 1);
                localStorage.setItem("menuA" + i + "n", this.names[i]) * 1;
            }
        }
    }
    localStorageClearThis(){
        for(var i = 0; i < 12; i++){
            localStorage.removeItem("menuA" + i + "s");
            localStorage.removeItem("menuA" + i + "n");
        }
    }
    resetAll(){
        this.names = [];
        this.scores = [];
        this.localStorageClearThis();
        this.showRanks();
    }

    removeNum(num){
        num = num - 1;
        if(num < 0 || num > 11){
            return;
        }
        localStorage.removeItem("menuA" + num + "s");
        localStorage.removeItem("menuA" + num + "n");
        this.backupRanks();
        this.saveRanks();
    }
}