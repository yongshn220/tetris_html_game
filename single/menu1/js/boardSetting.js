function boardSetting(board){
    var curPlayer = "";
    if(board === "game-board1"){
        curPlayer = "a";
    }
    else{
        curPlayer = "b";
    }
    var cont = document.getElementById(board);
    var blockStyle = 1;
    for(var i = 0; i < 200; i++){
        if(blockStyle === 1){
            blockStyle = 2;
        }
        else{
            blockStyle = 1;
        }
        if((i%10) === 0){
            if(blockStyle === 1){
                blockStyle = 2;
            }
            else{
                blockStyle = 1;
            }
        }
        cont.innerHTML += '<div id="' + curPlayer + '' + i + '"></div>';
        var a = (i%10) * 30;
        var b =  Math.trunc(i/10) * 30;
        $("#" + curPlayer + i).css("left", (i%10) * 30);
        $("#" + curPlayer + i).css("top", Math.trunc(i/10) * 30);

    }
}