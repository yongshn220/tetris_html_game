
class Block{
    x;
    y;
    shape;
    num;
    hardDropped;
    fixed;
    sx;
    sy;

    constructor(){
        this.x = 3;
        this.y = 0;
        this.sx = 3;
        this.sy = 0; 
        this.randomBlock();
        this.hardDropped = false;
        this.fixed = false;
    }

    move(b){
        this.x = b.x;
        this.y = b.y;
        this.shape = b.shape;
    }

    shadowMove(b){
        this.sy = b.sy;
    }
    
    randomBlock(){
        this.num = Math.floor(Math.random() * 7 + 1);
       // this.num = 2;
        switch(this.num){
            case 1 : this.shape = shapes.one;
                     break;
            case 2 : this.shape = shapes.two;
                     break;
            case 3 : this.shape = shapes.three;
                     break;
            case 4 : this.shape = shapes.four;
                     break;
            case 5 : this.shape = shapes.five;
                     break;
            case 6 : this.shape = shapes.six;
                     break;
            case 7 : this.shape = shapes.seven;
                     break;
        }
    }
    rotate(block){
        let b = JSON.parse(JSON.stringify(block));
        if(!this.hardDropped){
            for(var i = 0; i < b.shape.length; i++){
                for(var j = 0; j < i; j++){
                    [b.shape[i][j], b.shape[j][i]] = [b.shape[j][i], b.shape[i][j]];
                }
            }
            b.shape.forEach(row => row.reverse());
        }
        return b;
    }
}

class Player{
    board;
    cache;
    name;
    block;
    nextBlock1;
    nextBlock2;
    nextBlock3;
    hold;
    KOs;
    clearLinesNum;
    totalClearLinesNum;

    constructor(name){
        this.name = name;
        this.block = new Block();
        this.nextBlock1 = new Block();
        this.nextBlock2 = new Block();
        this.nextBlock3 = new Block();
        this.newBoard();
        this.hold = undefined;
        this.clearLinesNum = 0;
        this.totalClearLinesNum = 0;
        this.KOs = 3;
    }

    setBlockOnBoard(){
        if(!this.block.fixed){
            for(var i = 0; i < this.block.shape.length; i++){
                for(var j = 0; j < this.block.shape.length; j++){
                    if(this.block.shape[i][j] > 0){
                        var x = this.block.x + j;
                        var y = this.block.y + i;
                        var sx = this.block.sx + j;
                        var sy = this.block.sy + i;
                        this.board[sy][sx] = 8;
                        this.board[y][x] = this.block.shape[i][j];
                    }
                }
            }
        }
        var b = JSON.parse(JSON.stringify(this.block));
        b.y++;
        if(!this.isValid(b)){
            this.clearLines();
        }  
    }


    showBoard(){
        for(var i = 0; i < 20; i++){
            for(var j = 0; j < 10; j++){
                var colorNum = this.board[i][j];
                var curPosToNum = posToNum(j, i);
                $("#" + this.name + curPosToNum).removeClass();
                if(colorNum > 0){                
                    $("#" + this.name + curPosToNum).addClass("block" + colorNum);
                }

           }
       }
    }

    resetBoard(){ 
        for(var i = 0; i < 20; i++){
            for(var j = 0; j < 10; j++){
                if(!this.cache[i][j]){
                    this.board[i][j] = 0;
                }
            }
        }
    }

    drop(){
        var b = JSON.parse(JSON.stringify(this.block));
        b.y++;
        if(this.isValid(b)){
            this.block.y++;
            return true;
        }
        else{
            this.fixBlock();
            if(this.isValid(this.nextBlock1)){
                //this.fixBlock();
                this.nextBlockEvent();

            }
            else{
                if(this.KOs === 0){
                    return false;
                }
                this.KOs--;
                this.KOEvent();
                this.showKOs();
            }
        }
        return true;
    }

    KOEvent(){
        this.board.forEach((row, y) => {
            if(row.every(value => value === 9 || value === 10)){
                this.board.splice(y, 1);
                this.board.unshift(Array(10).fill(0));
                this.cache.splice(y, 1);
                this.cache.unshift(Array(10).fill(false));
            }
        });
        if(this.isValid(this.nextBlock1)){
            this.nextBlockEvent();
        }
        
    }

    nextBlockEvent(){
        this.block = this.nextBlock1;
        this.nextBlock1 = this.nextBlock2;
        this.nextBlock2 = this.nextBlock3;
        this.nextBlock3 = new Block();
        this.showNextBlocks();
    }

    showKOs(){
        var kos = (document).getElementById("KOText" + this.name);
        kos.innerHTML = this.KOs;
    }

    holdBlock(){
        this.block.x = 3; this.block.sx = 3;
        this.block.y = 0; this.block.sy = 0;
        if(this.hold === undefined){
            this.hold = this.block;
            this.block = this.nextBlock1;
            this.nextBlock1 = this.nextBlock2;
            this.nextBlock2 = this.nextBlock3;
            this.nextBlock3 = new Block();
        }
        else{
            var tempBlock = this.block;
            this.block = this.hold;
            this.hold = tempBlock;

        }
        this.showHoldBlock();
        this.showNextBlocks();
    }

    showHoldBlock(){   
        $("#hold" + this.name).removeClass().addClass("full-block" + this.hold.num);
    }

    showNextBlocks(){
        $("#nb1" + this.name).removeClass().addClass("full-block" + this.nextBlock1.num);
        $("#nb2" + this.name).removeClass().addClass("full-block" + this.nextBlock2.num);
        $("#nb3" + this.name).removeClass().addClass("full-block" + this.nextBlock3.num);
    }

    showAttackBar(num){
        var height = num * 30;
        var top = 700 - height;
        $("#attack-bar" + this.name).css('height', height).css('top', top);
        
    }
    clearAttackBar(){
        $("#attack-bar" + this.name).css('height', 0).css('top', 0);
    }

    attackedLines(num){
        var b = JSON.parse(JSON.stringify(this.block));
        b.y++;

        for(var i = 0; i < num; i++){
            if(!this.isValid(b) && this.block.y > 0){
                this.block.y--;
                b.y--;                   
            }            
            this.board.shift();
            this.board.push(Array(10).fill(9));  
            this.cache.shift();
            this.cache.push(Array(10).fill(true));
            var randomBoom = Math.floor(Math.random() * 10)
            this.board[19][randomBoom] = 10;               
        }

    }

    fixBlock(){
        this.block.fixed = true;
        for(var i = 0; i < this.block.shape.length; i++){
            for(var j = 0; j < this.block.shape.length; j++){
                let x = this.block.x + j;
                let y = this.block.y + i;
                if(this.isInsideWall(x) && this.isAboveFloor(y)){
                    if(this.board[y][x] > 0 && this.board[y][x] <= 7 && !this.cache[y][x]){
                        this.cache[y][x] = true;
                    }
                    if(this.isAboveFloor(y + 1)){
                        if(this.block.shape[i][j] !== 0 && this.board[y + 1][x] === 10){
                             this.block.y++;
                             this.board.splice(y + 1, 1);
                             this.board.unshift(Array(10).fill(0));
                             this.cache.splice(y + 1, 1);
                             this.cache.unshift(Array(10).fill(false));
                        }
                    }
                }
            }
        }   
    }

    clearLines(){
        this.board.forEach((row, y) => {
            if(row.every(value => value > 0 && value !== 9)){
                this.fixBlock();
                this.block.fixed = true;
                this.block.hardDropped = true;
                this.board.splice(y, 1);
                this.board.unshift(Array(10).fill(0));
                this.cache.splice(y, 1);
                this.cache.unshift(Array(10).fill(false));
                this.clearLinesNum++;
                this.totalClearLinesNum++;
                this.showTotalClearLines();
                
            }

        });
    }

    showTotalClearLines(){
        var num = (document).getElementById("line-sent-text" + this.name);
        num.innerHTML = this.totalClearLinesNum;
        if(this.totalClearLinesNum < 10){
            $(num).css('color', '#3bb9ff');
        }
        else if(this.totalClearLinesNum >= 10 && this.totalClearLinesNum < 20){
            $(num).css('color', '#3e73ff');
        }
        else if(this.totalClearLinesNum >= 20 && this.totalClearLinesNum < 30){
            $(num).css('color', '#8d3bff');
        }
        else if(this.totalClearLinesNum >= 30 && this.totalClearLinesNum < 40){
            $(num).css('color', '#b609ff');
        }
        else if(this.totalClearLinesNum >= 40 && this.totalClearLinesNum < 50){
            $(num).css('color', '#de3bff');
        }
        else{
            $(num).css('color', 'rebeccapurple');      
        }
    }

    newBoard(){
        this.board = Array.from({length : 20}, () => Array(10).fill(0));
        this.cache = Array.from({length : 20}, () => Array(10).fill(false));
    }

    isValid(b){
        if(b.hardDropped){
            return false;
        }
        return b.shape.every((row, dy) => {
            return row.every((value, dx) => {
                let x = b.x + dx;
                let y = b.y + dy;
                return (value === 0 || (this.isInsideWall(x) && this.isAboveFloor(y) && this.isNotOcuppied(x, y)));
            })
        })
    }

    isShadowValid(b){
        return b.shape.every((row, dy) => {
            return row.every((value, dx) => {
                let x = b.sx + dx;
                let y = b.sy + dy;
                return (value === 0 || (this.isInsideWall(x) && this.isAboveFloor(y) && this.isNotOcuppied(x, y)));
            })
        })
    }

    shadowEvent(){
        this.block.sx = this.block.x;
        this.block.sy = this.block.y;
        var b = JSON.parse(JSON.stringify(this.block));
        while(this.isShadowValid(b)){
            this.block.shadowMove(b);
            b.sy++;
        }
    }

    isInsideWall(x){
        return (x >= 0 && x < 10);
    }
    isAboveFloor(y){
        return (y < 20);
    }
    isNotOcuppied(x, y){
        if(x == undefined || y == undefined){
            console.log("a");
        }
        return !this.cache[y][x];
    }
}

function posToNum(x, y){
    return x + (y * 10);
}

function boardCopy(A){
    
}

function isBoardFull(board){
    for(var i = 0; i < board[0].length; i++){
        if(board[0][i] != 0){
            return true;
        }
    }
    return false;
}


const shapes = {
    one: 
       [[0, 0, 0, 0],
        [1, 1, 1, 1],
        [0, 0, 0, 0],
        [0, 0, 0, 0]],

    two: 
       [[0, 0, 0],
        [0, 2, 2],
        [2, 2, 0]],

    three:
       [[3, 3, 0],
        [0, 3, 3],
        [0, 0, 0]],

    four:
       [[4, 4],
        [4, 4]],

    five:
       [[0, 0, 0],
        [5, 5, 5],
        [5, 0, 0]],

    six:
       [[0, 0, 0],
        [6, 6, 6],
        [0, 6, 0]],
        
    seven:
       [[0, 0, 0],
        [7, 7, 7],
        [0, 0, 7]]
}
Object.freeze(shapes);
