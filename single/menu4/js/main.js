var player1;
var storage;
var requestId;
var timeover = false;
var gamestart = false;

$(document).ready(function(){
    boardSetting("game-board1");
    storage = new Storage();
    startEvnetListener();
})

function startEvnetListener(){
    document.addEventListener('keydown', event =>{
        if(event.keyCode === 32){
            if(!gamestart){
                play();
            }
            else{
                window.location.reload();
            }
        }
    })
}

function resetRank(){
    var pw = prompt("Password", "");
    if(pw === "yongjung"){
        storage.resetAll();
        alert("Successfully reset.")
    }
    else if(pw === "remove pos"){
        var rn = prompt("Which position do you want to remove?", "");
        storage.removeNum(rn)
    }
    else{
        alert("Wrong password!");
    }
}
function play(){
    gamestart = true;
    $("#start").css('height', 0);
    $("#start li").css('font-size', 0);
    $("#new-game").css('height', 50);
    //timeStrat();
    var tempCount = 2;
    var countIda = (document).getElementById("counta");

    countIda.innerHTML = ".   3";
    var count = setInterval(function(){
        if(tempCount > 0){
            countIda.innerHTML = ".   " + tempCount;
            tempCount--;
        }
        else if(tempCount === 0){
            countIda.innerHTML = "start!";
            tempCount--;        
        }

        else{
            countIda.innerHTML = "";
            gameStart();
            clearInterval(count);
        }
    }, 1000);
}

function gameStart(){
    
    player1 = new Player("a");

    var isEnd = false;  
    player1.setBlockOnBoard();
    player1.showBoard();
    player1.showNextBlocks();
    player1.showKOs();
    addEventListener();
    animate();
    if(requestId){
        cancelAnimationFrame(animate);
    }
}

var KEY1 = {
    //player one
    LEFT : 37,
    UP : 38,
    RIGHT : 39,
    DOWN : 40,
    HARDDROP : 83,
    HOLD : 81
}

var keyOneState = {
    [KEY1.LEFT] : false,
    [KEY1.RIGHT] : false,
    [KEY1.DOWN] : false,
    [KEY1.UP] : false,
    [KEY1.HARDDROP] : false,
    [KEY1.HOLD] : false
}

var movesOne = {
    [KEY1.LEFT] : b => ({...b, x : b.x - 1}),
    [KEY1.RIGHT] : b => ({...b, x : b.x + 1}),
    [KEY1.DOWN] : b => ({...b, y : b.y + 1}),
    [KEY1.HARDDROP] : b => ({...b, y : b.y + 1}),
    [KEY1.UP] : b => b.rotate(b),
    [KEY1.HOLD] : true
}

function movesLoop(player, skey){
    var movesTemp = movesOne;
    var keyTemp = KEY1;
    var keyStateTemp = keyOneState;
    let b;

    if(!keyStateTemp[skey]){
        return;
    }

    if(skey === keyTemp.HOLD){
        player.holdBlock();
        player.shadowEvent();
        player.setBlockOnBoard();
        keyStateTemp[skey] = false;
        return;
    }

    b = movesTemp[skey](player.block);

    if(skey === keyTemp.HARDDROP){
        while(player.isValid(b)){
           player.block.move(b);
           b = movesTemp[skey](player.block);            
        }
        player.resetBoard();
        //player.fixBlock();
        player.setBlockOnBoard();
        player.showBoard();
        player.block.hardDropped = true;
        keyStateTemp[skey] = false;
        return;
    }
    else if(player.isValid(b)){
        player.block.move(b);
        player.resetBoard();
        player.shadowEvent();   
        player.setBlockOnBoard();           
    }
    
    if(skey === keyTemp.DOWN){
        setTimeout(function(){
            movesLoop(player, skey);
        }, 20);    
    }
}

function addEventListener(){
    document.addEventListener('keydown', event =>{
        if(movesOne[event.keyCode] && !keyOneState[event.keyCode]){
            event.preventDefault();
            keyOneState[event.keyCode] = true;
            movesLoop(player1, event.keyCode);
        }
    })

    document.addEventListener('keyup', event => {
        if(movesOne[event.keyCode]){
            keyOneState[event.keyCode] = false;
        }
    })
}

time1 = {
    start : 0,
    elapsed : 0,
    level : 1000
}


function animate(now = 0){
    time1.elapsed = now - time1.start;

    if(time1.elapsed > time1.level || player1.block.hardDropped){
        time1.start = now;
        if(!player1.drop()){
            var name = prompt("Timeout. Enter your name.", "");
            while(name === "" || name === null){
                if(name === null){
                    return;
                }
                name = prompt("Please enter your name again.");
            }
            storage.addRank(name,player1.totalClearLinesNum )
            storage.showRanks();
            return;
        }
    }
    player1.shadowEvent();
    player1.resetBoard();
    player1.setBlockOnBoard();  
    player1.showBoard();
    requestId = requestAnimationFrame(animate);
}
