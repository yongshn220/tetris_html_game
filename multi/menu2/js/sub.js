
class Block{
    x;
    y;
    shape;
    num;
    hardDropped;
    fixed;
    sx;
    sy;

    constructor(num){
        this.x = 3;
        this.y = -1;
        this.sx = 3;
        this.sy = 0;
        this.randomBlock(num);
        this.itemBlockEvent();
        this.setPosition();
        this.hardDropped = false;
        this.fixed = false;
    }
    
    setPosition(){
        if(this.num === 3 || this.num === 4){
            this.y = 0;
        }
    }
    move(b){
        this.x = b.x;
        this.y = b.y;
        this.shape = b.shape;
    }

    shadowMove(b){
        this.sy = b.sy;
    }
    
    randomBlock(num){
        if(num === undefined){
            this.num = Math.floor(Math.random() * 7 + 1);
        }
        else{
            this.num = num;
        }
        //this.num = 1;
        switch(this.num){
            case 1 : this.shape = this.copyShape(shapes.one);
                     break;
            case 2 : this.shape = this.copyShape(shapes.two);
                     break;
            case 3 : this.shape = this.copyShape(shapes.three);
                     break;
            case 4 : this.shape = this.copyShape(shapes.four);
                     break;
            case 5 : this.shape = this.copyShape(shapes.five);
                     break;
            case 6 : this.shape = this.copyShape(shapes.six);
                     break;
            case 7 : this.shape = this.copyShape(shapes.seven);
                     break;
        }
    }

    copyShape(shape){
        let newShape = [];
        for(let i = 0; i < shape.length; i++){
            let shapeLine = [];
            for(let j = 0; j < shape[0].length; j++){
                shapeLine[j] = shape[i][j];
            }
            newShape[i] = shapeLine;
        }
        return newShape;
    }
    
    rotate(block){
        let b = JSON.parse(JSON.stringify(block));
        if(!this.hardDropped){
            for(var i = 0; i < b.shape.length; i++){
                for(var j = 0; j < i; j++){
                    [b.shape[i][j], b.shape[j][i]] = [b.shape[j][i], b.shape[i][j]];
                }
            }
            b.shape.forEach(row => row.reverse());
        }
        return b;
    }

    itemBlockEvent(){
        let randomNum = Math.random();
        if(randomNum > 0.9){
            this.setItemPiece();
        }
    }
    
    setItemPiece(){
        let num = Math.floor(Math.random() * 5 + 11);
        //num = 14;
        for(let i = 0; i < this.shape.length; i++){
            for(let j = 0; j < this.shape[0].length; j++){
                if(this.shape[i][j] !== 0){
                    this.shape[i][j] = num;
                    return;
                }
            }
        }
    }
}

class Player{
    board;
    oppoBoard;
    cache;
    oppoCache;
    name;
    block;
    savedBlock;
    nextBlock1;
    nextBlock2;
    nextBlock3;
    hold;
    holdUsed;
    KOs;
    isEnd;
    clearLinesNum;
    totalClearLinesNum;
    combo;

    constructor(name){
        this.name = name;
        this.block = new Block();
        this.nextBlock1 = new Block();
        this.nextBlock2 = new Block();
        this.nextBlock3 = new Block();
        this.newBoard();
        this.hold = undefined;
        this.holdUsed = false;
        this.clearLinesNum = 0;   
        this.totalClearLinesNum = 0;
        this.KOs = 3;
        this.isEnd = false;
        this.combo = 0;
        this.itemList = [];
    }

    setOppoBoard(oppoBoard){
        this.oppoBoard = oppoBoard;
    }
    setOppoCache(oppoCache){
        this.oppoCache = oppoCache;
    }

    setBlockOnBoard(){
        if(!this.block.fixed){
            for(var i = 0; i < this.block.shape.length; i++){
                for(var j = 0; j < this.block.shape.length; j++){
                    if(this.block.shape[i][j] > 0){
                        var x = this.block.x + j;
                        var y = this.block.y + i;
                        var sx = this.block.sx + j;
                        var sy = this.block.sy + i;
                        this.board[sy][sx] = 8;
                        this.board[y][x] = this.block.shape[i][j];
                    }
                }
            }
        }
        var b = JSON.parse(JSON.stringify(this.block));
        b.y++;
        if(!this.isValid(b)){
           // this.clearLines();
        }  
    }


    showBoard(){
        for(var i = 0; i < 20; i++){
            for(var j = 0; j < 10; j++){
                var colorNum = this.board[i][j];
                var curPosToNum = posToNum(j, i);
                $("#" + this.name + curPosToNum).removeClass();
                if(colorNum > 0){                
                    $("#" + this.name + curPosToNum).addClass("block" + colorNum);
                }

           }
       }
    }

    resetBoard(){ 
        for(var i = 0; i < 20; i++){
            for(var j = 0; j < 10; j++){
                if(!this.cache[i][j]){
                    this.board[i][j] = 0;
                }
            }
        }
    }

    drop(){
        var b = JSON.parse(JSON.stringify(this.block));
        b.y++;
        if(this.isValid(b)){
            this.block.y++;
        }
        else{
            this.fixBlock();
            if(!this.KOCheck()){
                this.nextBlockEvent();
            }
            else{
                this.KOEvent();
            }
        }
    }
    EndEvent(){
        this.board.forEach((line, y) =>{
            line.forEach((value, x) =>{
                if(value > 0 && value < 11){
                    this.board[y][x] = 9;
                }
            })
        })
    }
    
    isEndCheck(){
        if(this.KOs < 0){
            this.isEnd = true;
            this.EndEvent();
        }
        else{
            this.isEnd = false;
            this.showKOs();
        }
        return this.isEnd;
    }

    KOEvent(){
        this.KOs--;
        if(this.isEndCheck()){
            return;
        }
        this.board.forEach((row, y) => {
            if(row.every(value => value === 9 || value === 10)){
                this.board.splice(y, 1);
                this.board.unshift(Array(10).fill(0));
                this.cache.splice(y, 1);
                this.cache.unshift(Array(10).fill(false));
            }
        });
        if(this.block.fixed && !this.KOCheck()){
            this.nextBlockEvent();
        }
    }


    nextBlockEvent(){
        this.block = this.nextBlock1;
        this.nextBlock1 = this.nextBlock2;
        this.nextBlock2 = this.nextBlock3;
        this.nextBlock3 = new Block();
        this.showNextBlocks();
    }

    showKOs(){
        var kos = (document).getElementById("KOText" + this.name);
        kos.innerHTML = this.KOs;
    }

    holdBlock(){
        if(this.holdUsed){
            return;
        }
        this.block = new Block(this.block.num);
        if(this.hold === undefined){
            this.hold = this.block;
            this.block = this.nextBlock1;
            this.nextBlock1 = this.nextBlock2;
            this.nextBlock2 = this.nextBlock3;
            this.nextBlock3 = new Block();
        }
        else{
            var tempBlock = this.block;
            this.block = this.hold;
            this.hold = tempBlock;

        }
        this.showHoldBlock();
        this.showNextBlocks();
        this.holdUsed = true;
    }

    showHoldBlock(){   
        $("#hold" + this.name).removeClass().addClass("full-block" + this.hold.num);
    }

    showNextBlocks(){
        $("#nb1" + this.name).removeClass().addClass("full-block" + this.nextBlock1.num);
        $("#nb2" + this.name).removeClass().addClass("full-block" + this.nextBlock2.num);
        $("#nb3" + this.name).removeClass().addClass("full-block" + this.nextBlock3.num);
    }

    showAttackBar(num){
        var height = num * 30;
        var top = 700 - height;
        $("#attack-bar" + this.name).css('height', height).css('top', top);
        
    }
    clearAttackBar(){
        $("#attack-bar" + this.name).css('height', 0).css('top', 0);
    }

    attackedLines(num){
        let isBlockSaved = false;
        var b = JSON.parse(JSON.stringify(this.block));
        b.y += 2;
        let attackedNum = 0;

        for(var i = 0; i < num; i++){
            if(!this.isValid(b) && !isBlockSaved){
                while(!this.isValid(b) && !isBlockSaved){
                    if(this.block.y <= 0){
                        this.savedBlock = this.block;
                        isBlockSaved = true;
                        break;
                    }
                    this.block.y--;
                    b.y--;
                }
            }               
            
                      
            this.board.shift();
            this.board.push(Array(10).fill(9));  
            this.cache.shift();
            this.cache.push(Array(10).fill(true));
            var randomBoom = Math.floor(Math.random() * 10);
            //var randomBoom = 5;
            this.board[19][randomBoom] = 10;
            attackedNum++;
            
            if(this.KOCheck()){
                this.KOEvent();
                if(isBlockSaved){
                    this.block = this.savedBlock;  
                }
                break;
            } 
        }
        return num - attackedNum ;
    }

    KOCheck(){
        for(let i = 0; i < 10; i++){
            if(this.board[2][i] !== 0 && this.cache[2][i]){
                return true;
            }
        }
        return false;
    }

    fixBlock(){
        this.holdUsed = false; // reset hold
        this.block.fixed = true;
        let isAttackLineDeleted = false;
        for(var i = 0; i < this.block.shape.length; i++){
            for(var j = 0; j < this.block.shape.length; j++){
                let x = this.block.x + j;
                let y = this.block.y + i;
                if(this.isInsideWall(x) && this.isAboveFloor(y)){
                    if(this.isValidToFixBlock(x, y)){
                        this.cache[y][x] = true;
                    }
                    if(this.isAboveFloor(y + 1)){
                        if(this.block.shape[i][j] !== 0 && this.board[y + 1][x] === 10 && !isAttackLineDeleted){
                             this.block.y++;
                             this.board.splice(y + 1, 1);
                             this.board.unshift(Array(10).fill(0));
                             this.cache.splice(y + 1, 1);
                             this.cache.unshift(Array(10).fill(false));
                             isAttackLineDeleted = true;
                        }
                    }
                }
            }
        }
        this.comboUpdate();
    }

    isValidToFixBlock(x, y){
        let isNormal = this.isNormalBlock(this.board[y][x]);
        let isItem = this.isItemBlock(this.board[y][x]);
        return ((isNormal || isItem) && !this.cache[y][x])
    }

    isItemBlock(piece){
        return (piece >= 11 && piece <= 15);
    }
    isNormalBlock(piece){
        return (piece >= 1 && piece <= 7)
    }

    comboUpdate(){
        if(this.clearLines()){
            this.combo++;
        }
        else{
            this.combo = 0;
        }
        this.comboEvent(); 
    }

    comboEvent(){
        if(this.combo !== 0 && this.combo % 2 === 0){
            this.clearLinesNum += 1;
            this.totalClearLinesNum += 1;
        }

        this.showTotalClearLines();
    }

    clearLines(){
        let isAnyBlockCleared = false;
        this.board.forEach((row, y) => {
            if(row.every(value => value > 0 && value !== 9)){
                this.itemCheckInRow(row);
                this.block.fixed = true;
                this.block.hardDropped = true;
                this.board.splice(y, 1);
                this.board.unshift(Array(10).fill(0));
                this.cache.splice(y, 1);
                this.cache.unshift(Array(10).fill(false));
                this.clearLinesNum++;
                //this.clearLinesNum = 10;
                this.totalClearLinesNum++;
                this.showTotalClearLines();
                isAnyBlockCleared = true;
            }
        });
        return isAnyBlockCleared;
    }

    itemCheckInRow(row){
        row.forEach(value => {
            if(this.isItemBlock(value)){
                this.itemList.push(new Item(this.name, value, this.board, this.cache, this.oppoBoard, this.oppoCache));
                
            }
        })
    }

    itemExecute(){
        this.itemList.forEach(item => item.execute());
    }

    itemTimer(){
        this.itemList.forEach(item => item.timer());
        this.itemTimeOver();
    }

    itemTimeOver(){
        this.itemList = this.itemList.filter(item => !item.timeOver);
    }

    showTotalClearLines(){
        var num = (document).getElementById("line-sent-text" + this.name);
        num.innerHTML = this.totalClearLinesNum;
        if(this.totalClearLinesNum < 10){
            $(num).css('color', '#ff7676');
        }
        else if(this.totalClearLinesNum >= 10 && this.totalClearLinesNum < 20){
            $(num).css('color', '#ff9325');
        }
        else if(this.totalClearLinesNum >= 20 && this.totalClearLinesNum < 30){
            $(num).css('color', '#abd500');
        }
        else if(this.totalClearLinesNum >= 30 && this.totalClearLinesNum < 40){
            $(num).css('color', '#6cf2ff');
        }
        else if(this.totalClearLinesNum >= 40 && this.totalClearLinesNum < 50){
            $(num).css('color', '#b609ff');
        }
        else{
            $(num).css('color', '##1bf5ff');      
        }
    }

    newBoard(){
        this.board = Array.from({length : 20}, () => Array(10).fill(0));
        this.cache = Array.from({length : 20}, () => Array(10).fill(false));
    }

    isValid(b){
        if(b.hardDropped){
            return false;
        }
        return b.shape.every((row, dy) => {
            return row.every((value, dx) => {
                let x = b.x + dx;
                let y = b.y + dy;
                return (value === 0 || (this.isInsideWall(x) && this.isAboveFloor(y) && this.isNotOcuppied(x, y)));
            })
        })
    }

    isShadowValid(b){
        return b.shape.every((row, dy) => {
            return row.every((value, dx) => {
                let x = b.sx + dx;
                let y = b.sy + dy;
                return (value === 0 || (this.isInsideWall(x) && this.isAboveFloor(y) && this.isNotOcuppied(x, y)));
            })
        })
    }

    shadowEvent(){
        this.block.sx = this.block.x;
        this.block.sy = this.block.y;
        var b = JSON.parse(JSON.stringify(this.block));
        while(this.isShadowValid(b)){
            this.block.shadowMove(b);
            b.sy++;
        }
    }

    showComboBar(){
        $("#combo" + this.name).removeClass().addClass("combo-" + this.combo);
        $("#canvas" + this.name).css("border", `5px solid ${this.getColorByCombo()}`);
    }

    getColorByCombo(){
        switch(this.combo){
            case 0 : return "#828282";
            case 1 : return "#828282";
            case 2 : return "#FFC200";
            case 3 : return "#FFA700";
            case 4 : return "#FF8000";
            case 5 : return "#FF5D00";
            case 6 : return "#FF4600";
            case 7 : return "#FF1F00";
            case 8 : return "#FF0000";
            case 9 : return "#CD0000";
            default : return "#CD0000";
        }
    }
    

    isInsideWall(x){
        return (x >= 0 && x < 10);
    }

    isAboveFloor(y){
        return (y >= 0  && y < 20); // above floor and below top;
    }
    isNotOcuppied(x, y){
        if(x == undefined || y == undefined){
            console.log("a");
        }
        return !this.cache[y][x];
    }

}

function posToNum(x, y){
    return x + (y * 10);
}

function boardCopy(A){
    
}

function isBoardFull(board){
    for(var i = 0; i < board[0].length; i++){
        if(board[0][i] != 0){
            return true;
        }
    }
    return false;
}


const shapes = {
    one: 
       [[0, 0, 0, 0],
        [1, 1, 1, 1],
        [0, 0, 0, 0],
        [0, 0, 0, 0]],

    two: 
       [[0, 0, 0],
        [0, 2, 2],
        [2, 2, 0]],

    three:
       [[3, 3, 0],
        [0, 3, 3],
        [0, 0, 0]],

    four:
       [[4, 4],
        [4, 4]],

    five:
       [[0, 0, 0],
        [5, 5, 5],
        [5, 0, 0]],

    six:
       [[0, 0, 0],
        [6, 6, 6],
        [0, 6, 0]],
        
    seven:
       [[0, 0, 0],
        [7, 7, 7],
        [0, 0, 7]]
}
Object.freeze(shapes);
