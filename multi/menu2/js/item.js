class Item{
    name;
    num;
    board;
    curTime;
    maxTime;
    canvas;
    oppoCanvas;
    oppoDropTime;
    oppoKey;
    oppoBoard;
    timeOver;
    executed;


    constructor(name, num, board, cache, oppoBoard, oppoCache){
        this.name = name;
        this.num = num;
        this.board = board;
        this.cache = cache;
        this.oppoBoard = oppoBoard;
        this.oppoCache = oppoCache;
        this.curTime = 0;
        this.maxTime = 0;
        this.timeOver = false;
        this.executed = false;
        this.img = new Image();
        this.img.src = 'css/images/item-11-effect.png';
        this.setting();
    }

    
    
    setting(){
        if(this.name === 'a'){
            this.canvas = canvasA;
            this.oppoDropTime = time2;
            this.oppoKey = KEY2;
            this.oppoCanvas = canvasB;
        }
        else{
            this.canvas = canvasB;
            this.oppoDropTime = time1;
            this.oppoKey = KEY1;
            this.oppoCanvas = canvasA;
        }
    }
    timer(){
        this.curTime++;
        this.timeCheck();
    }

    timeCheck(){
        if(this.maxTime <= this.curTime){
            this.timeOver = true;
            this.resetEffect();
        }
        else{
            this.timeOver = false;
        }
    }

    resetEffect(){
        time1.level = 1000;
        time2.level = 1000;
    }
    
    orderingBlocks(){
        this.board.forEach((row, y) => {
            let oLine = row.filter((value, x) => value != 0 && this.cache[y][x]);
            while(oLine.length < 10){
                oLine.push(0);
            }
            this.board[y] = oLine;
        });

        this.cache.forEach((row, y) => {
            let oLine = row.filter(value => value);
            while(oLine.length < 10){
                oLine.push(false);
            }
            this.cache[y] = oLine;
        });
    }

    removeAttackedBlocks(){
        this.board.forEach((row, y) => {
           if(row.every(value => value === 9 || value === 10)){
               this.board.splice(y, 1);
               this.board.unshift(Array(10).fill(0));
               this.cache.splice(y, 1);
               this.cache.unshift(Array(10).fill(false));
           } 
        });
    }

    removeRandomPiece(){
        this.oppoBoard.forEach((row,y) => {
            let rNum = Math.floor(Math.random() * 10);
            let tmpCache = this.oppoCache[y][rNum];
            let tmpPiece = this.oppoBoard[y][rNum];
            if(tmpCache && tmpPiece !== 9 && tmpPiece !== 10){
                this.oppoBoard[y][rNum] = 0;
                this.oppoCache[y][rNum] = false;
            }
            
        })
    }

    execute(){
        if(this.timeOver){
            return;
        }
        if(!this.executed){
            switch(this.num){
                //상대방 화면 가리기
                case 11 :{
                    this.maxTime = 3;
                    this.oppoCanvas.context.drawImage(this.img,0,0,300,600,0,0,300,600);
                }
                break;
                // 상대방 블럭 속도 up
                case 12 :{
                    this.maxTime = 3;
                    this.oppoDropTime.level = 100; 
                }
                break;
                //상대방 블럭 랜덤으로 지우기
                case 13 :{
                    this.timeOver = true;
                    this.removeRandomPiece();
                }
                break;
                // 블럭 한쪽 정렬
                case 14 :{
                    this.timeOver = true;
                    this.orderingBlocks();
                }
                break;
                //attacted block 지우기
                case 15 :{
                    this.timeOver = true;
                    this.removeAttackedBlocks();
                }
                break;
            }
        }
            
    }
}