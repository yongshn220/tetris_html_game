/* 
20.12.17
 update :  .. hardDropped 시 블럭 사라지는 현상 수정.
           .. combo background effect 추가.

20.12.10 
 update :  .. hold 횟수 제한, 
           .. 블럭 강제 고정 되는 버그 보완,
           .. 콤보 보너스 증가 
*/

var player1;
var player2;
var requestId;
var gamestart;
var canvasA = new Canvas("a");
var canvasB = new Canvas("b");
$(document).ready(function(){
    gamestart = false;
    boardSetting("game-board1");
    boardSetting("game-board2");
    startEvnetListener();
   
})

function startEvnetListener(){
    document.addEventListener('keydown', event =>{
        if(event.keyCode === 32){
            if(!gamestart){
                gamestart = true;
                play();
            }
            else{
                window.location.reload();
            }
        }
    })
}


function play(){
    $("#msgA").css('font-size', 0);
    $("#msgB").css('font-size', 0);
    var tempCount = 2;
    var countIda = (document).getElementById("counta");
    var countIdb = (document).getElementById("countb");

    countIda.innerHTML = "3";
    countIdb.innerHTML = "3";
    var count = setInterval(function(){
        if(tempCount > 0){
            countIda.innerHTML = tempCount;
            countIdb.innerHTML = tempCount;
            tempCount--;
        }
        else if(tempCount === 0){
            countIda.innerHTML = "start!";
            countIdb.innerHTML = "start!";
            tempCount--;        
        }

        else{
            countIda.innerHTML = "";
            countIdb.innerHTML = "";
            gameStart();
            clearInterval(count);
        }
    }, 1000);
}

function gameStart(){
    
    player1 = new Player("a");
    player2 = new Player("b");

    var isEnd = false;  
    player1.setBlockOnBoard();
    player2.setBlockOnBoard();
    player1.showBoard();
    player2.showBoard();
    player1.showNextBlocks();
    player2.showNextBlocks();
    player1.showKOs();
    player2.showKOs();
    addEventListener();
    animate();
    if(requestId){
        cancelAnimationFrame(animate);
    }
}

var KEY1 = {
    //player one
    LEFT : 68,
    UP : 82,
    RIGHT : 71,
    DOWN : 70,
    HARDDROP : 83,
    HOLD : 81
}


var KEY2 = {
    //player two
    LEFT : 37,
    UP : 38,
    RIGHT : 39,
    DOWN : 40,
    HARDDROP : 186,
    HOLD : 79
}

var keyOneState = {
    [KEY1.LEFT] : false,
    [KEY1.RIGHT] : false,
    [KEY1.DOWN] : false,
    [KEY1.UP] : false,
    [KEY1.HARDDROP] : false,
    [KEY1.HOLD] : false
}

var keyTwoState = {
    [KEY2.LEFT] : false,
    [KEY2.RIGHT] : false,
    [KEY2.DOWN] : false,
    [KEY2.UP] : false,
    [KEY2.HARDDROP] : false,
    [KEY2.HOLD] : false
}

var movesOne = {
    [KEY1.LEFT] : b => ({...b, x : b.x - 1}),
    [KEY1.RIGHT] : b => ({...b, x : b.x + 1}),
    [KEY1.DOWN] : b => ({...b, y : b.y + 1}),
    [KEY1.HARDDROP] : b => ({...b, y : b.y + 1}),
    [KEY1.UP] : b => b.rotate(b),
    [KEY1.HOLD] : true
}

var movesTwo = {
    [KEY2.LEFT] : b => ({...b, x : b.x - 1}),
    [KEY2.RIGHT] : b => ({...b, x : b.x + 1}),
    [KEY2.DOWN] : b => ({...b, y : b.y + 1}),
    [KEY2.HARDDROP] : b => ({...b, y : b.y + 1}),
    [KEY2.UP] :b => b.rotate(b),
    [KEY2.HOLD] : true
}
function movesLoop(player, skey){
    var movesTemp;
    var KEY;
    var keyTemp;
    var keyStateTemp;
    let b;

    if(player.name === 'a'){
        movesTemp = movesOne;
        keyTemp = KEY1;
        keyStateTemp = keyOneState;
    }
    else{
        movesTemp = movesTwo;
        keyTemp = KEY2;
        keyStateTemp = keyTwoState;
    }

    if(!keyStateTemp[skey]){
        return;
    }

    if(skey === keyTemp.HOLD){
        player.holdBlock();
        player.shadowEvent();
        player.setBlockOnBoard();
        keyStateTemp[skey] = false;
        return;
    }

    b = movesTemp[skey](player.block);

    if(skey === keyTemp.HARDDROP){
        while(player.isValid(b)){
            player.block.move(b);
            player.resetBoard();
            player.setBlockOnBoard();
            player.showBoard();
            b = movesTemp[skey](player.block);            
        }
        player.block.hardDropped = true;
        player.drop(); // to fix hardDropped block and call nextBlockEvent function.
        keyStateTemp[skey] = false;
        return;
    }
    if(skey === keyTemp.UP && !player.isValid(b)){
        b = movesTemp[keyTemp.DOWN](b);
    }
    if(player.isValid(b)){
        player.block.move(b);
        player.resetBoard();
        player.shadowEvent();   
        player.setBlockOnBoard();           
    }
    
    if(skey === keyTemp.DOWN){
        setTimeout(function(){
            movesLoop(player, skey);
        }, 30);    
    }
}

function addEventListener(){
    document.addEventListener('keydown', event =>{
        console.log(event.keyCode);
        if(movesOne[event.keyCode] && !keyOneState[event.keyCode]){
            event.preventDefault();
            keyOneState[event.keyCode] = true;
            movesLoop(player1, event.keyCode);
        }
        else if(movesTwo[event.keyCode] && !keyTwoState[event.keyCode]){
            event.preventDefault();
            keyTwoState[event.keyCode] = true;
            movesLoop(player2, event.keyCode); 
        }
    })

    document.addEventListener('keyup', event => {
        if(movesOne[event.keyCode]){
            keyOneState[event.keyCode] = false;
        }
        else if(movesTwo[event.keyCode]){
            keyTwoState[event.keyCode] = false;
        }
    })
}

time1 = {
    start : 0,
    elapsed : 0,
    level : 1000
}

time2 = {
    start : 0,
    elapsed : 0,
    level : 1000
}

barTime1 = {
    start : 0,
    elapsed : 0,
    level : 1000,
    state : false
}

barTime2 = {
    start : 0,
    elapsed : 0,
    level : 1000,
    state : false
}




function animate(now = 0){
    canvasA.clearCanvas();
    canvasB.clearCanvas();
    time1.elapsed = now - time1.start;
    time2.elapsed = now - time2.start;
    barTime1.elapsed = now - barTime1.start;
    barTime2.elapsed = now - barTime2.start;

    player1.showBackgroundEffect();
    player2.showBackgroundEffect();
    
    clearLineBarEvent(now);
    clearLineEvent(now);
    keyDownPressEvent(now);
    dropEvent(now);

    playersAnimationEvent();

    if(isGameEnd()){
        return;
    }
    //canvasA.drawRect();
    requestId = requestAnimationFrame(animate);
}


function playersAnimationEvent(){
    player1.shadowEvent();
    player1.resetBoard();
    player1.setBlockOnBoard();  
    player1.showBoard();
    player1.showComboBar();
    

    player2.shadowEvent();
    player2.resetBoard();
    player2.setBlockOnBoard();  
    player2.showBoard();
    player2.showComboBar();
}

function isGameEnd(){
    if(player1.isEnd){
        gameEndEvent(canvasB, canvasA);
        return true;
    }
    else if(player2.isEnd){
        gameEndEvent(canvasA, canvasB);
        return true;
    }
    else{
        return false;
    }
}

function gameEndEvent(winC, loseC){
    winC.context.font = "60px Arial";
    winC.context.textAlign = "center";
    winC.context.fillStyle = "white";
    winC.context.fillText("Win!", 150, 300);
    winC.context.font = "15px Arial";
    winC.context.fillText("Press 'space bar' to restart.", 150, 330);

    loseC.context.font = "60px Arial";
    loseC.context.textAlign = "center";
    loseC.context.fillStyle = "white";
    loseC.context.fillText("Lose..", 150, 300);
    loseC.context.font = "15px Arial";
    loseC.context.fillText("Press 'space bar' to restart.", 150, 330);
}


function clearLineBarEvent(now){
    if(player1.clearLinesNum > 0){
        player2.showAttackBar(player1.clearLinesNum);
        if(!barTime2.state){
            barTime2.start = now;
            barTime2.elapsed = 0;
            barTime2.state = true;
        } 
    }

    if(player2.clearLinesNum > 0){
        player1.showAttackBar(player2.clearLinesNum);
        if(!barTime1.state){
            barTime1.start = now;
            barTime1.elapsed = 0;
            barTime1.state = true;
        }
    }
}

function clearLineEvent(now){

    if(player1.clearLinesNum > 0 && barTime2.elapsed > barTime2.level && barTime2.state){
            let restAttackLineA = player2.attackedLines(player1.clearLinesNum);
            player2.clearAttackBar();
            player2.shadowEvent(); 
            player1.clearLinesNum = restAttackLineA;
            barTime2.state = false;

    }

    if(player2.clearLinesNum > 0 && barTime1.elapsed > barTime1.level && barTime1.state){
            let restAttackLineB = player1.attackedLines(player2.clearLinesNum); 
            player1.clearAttackBar();
            player1.shadowEvent();
            player2.clearLinesNum = restAttackLineB;
            barTime1.state = false;
    }
}

function keyDownPressEvent(now){
    if(keyOneState[KEY1.DOWN]){
        time1.start = now;
    }
    if(keyTwoState[KEY2.DOWN]){
        time2.start = now;
    }
}

function dropEvent(now){
    if(time1.elapsed > time1.level){
        time1.start = now;
        player1.drop();
    }
        
    if(time2.elapsed > time2.level){
        time2.start = now;
        
        player2.drop();
    }
}
