
class Block{
    x;
    y;
    shape;
    num;
    hardDropped;
    fixed;
    sx;
    sy;

    constructor(num){
        this.x = 3;
        this.y = -1;
        this.sx = 3;
        this.sy = 0; 
        this.randomBlock(num);
        this.setPosition();
        this.hardDropped = false;
        this.fixed = false;

    }
    
    setPosition(){
        if(this.num === 3 || this.num === 4){
            this.y = 0;
        }
    }
    move(b){
        this.x = b.x;
        this.y = b.y;
        this.shape = b.shape;
    }

    shadowMove(b){
        this.sy = b.sy;
    }
    
    randomBlock(num){
        if(num === undefined){
            this.num = Math.floor(Math.random() * 7 + 1);
        }
        else{
            this.num = num;
        }
        //this.num = 7;
        switch(this.num){
            case 1 : this.shape = shapes.one;
                     break;
            case 2 : this.shape = shapes.two;
                     break;
            case 3 : this.shape = shapes.three;
                     break;
            case 4 : this.shape = shapes.four;
                     break;
            case 5 : this.shape = shapes.five;
                     break;
            case 6 : this.shape = shapes.six;
                     break;
            case 7 : this.shape = shapes.seven;
                     break;
        }
    }
    rotate(block){
        let b = JSON.parse(JSON.stringify(block));
        if(!this.hardDropped){
            for(var i = 0; i < b.shape.length; i++){
                for(var j = 0; j < i; j++){
                    [b.shape[i][j], b.shape[j][i]] = [b.shape[j][i], b.shape[i][j]];
                }
            }
            b.shape.forEach(row => row.reverse());
        }
        return b;
    }
}

class Player{
    board;
    cache;
    name;
    block;
    savedBlock;
    nextBlock1;
    nextBlock2;
    nextBlock3;
    hold;
    holdUsed;
    KOs;
    isEnd;
    clearLinesNum;
    totalClearLinesNum;
    combo;

    constructor(name){
        this.name = name;
        this.block = new Block();
        this.nextBlock1 = new Block();
        this.nextBlock2 = new Block();
        this.nextBlock3 = new Block();
        this.newBoard();
        this.hold = undefined;
        this.holdUsed = false;
        this.clearLinesNum = 0;   
        this.totalClearLinesNum = 0;
        this.KOs = 3;
        this.isEnd = false;
        this.combo = 0;
        this.setCanvas();
        this.BEImage = new MovingImage('css/images/backgroundEffect.png', this.canvas, 8);
        this.BEImage.setInformation(300,0,300,600,0,0,300,600,1200);
    }
    
    //canvas function
    setCanvas(canvas){
        if(this.name === "a"){
            this.canvas = canvasA;
        }
        else{
            this.canvas = canvasB;
        }
    }
    
    // drop function
    drop(){
        var b = JSON.parse(JSON.stringify(this.block));
        b.y++;
        if(this.isValid(b)){
            this.block.y++;
        }
        else{
            this.fixBlock();
            if(!this.KOCheck()){
                this.nextBlockEvent();
            }
            else{
                this.KOEvent();
            }
        }
    }

    //End event function
    EndEvent(){
        this.board.forEach((line, y) =>{
            line.forEach((value, x) =>{
                if(value > 0 && value < 11){
                    this.board[y][x] = 9;
                }
            })
        })
    }
    
    isEndCheck(){
        if(this.KOs < 0){
            this.isEnd = true;
            this.EndEvent();
        }
        else{
            this.isEnd = false;
            this.showKOs();
        }
        return this.isEnd;
    }
    
    //KO Event function
    KOEvent(){
        this.KOs--;
        if(this.isEndCheck()){
            return;
        }
        this.board.forEach((row, y) => {
            if(row.every(value => value === 9 || value === 10)){
                this.board.splice(y, 1);
                this.board.unshift(Array(10).fill(0));
                this.cache.splice(y, 1);
                this.cache.unshift(Array(10).fill(false));
            }
        });
        if(this.block.fixed && !this.KOCheck()){
            this.nextBlockEvent();
        }
    }

    showKOs(){
        var kos = (document).getElementById("KOText" + this.name);
        kos.innerHTML = this.KOs;
    }
    KOCheck(){
        for(let i = 0; i < 10; i++){
            if(this.board[2][i] !== 0 && this.cache[2][i]){
                return true;
            }
        }
        return false;
    }

    //hold block function
    holdBlock(){
        if(this.holdUsed){
            return;
        }
        this.block = new Block(this.block.num);
        if(this.hold === undefined){
            this.hold = this.block;
            this.block = this.nextBlock1;
            this.nextBlock1 = this.nextBlock2;
            this.nextBlock2 = this.nextBlock3;
            this.nextBlock3 = new Block();
        }
        else{
            var tempBlock = this.block;
            this.block = this.hold;
            this.hold = tempBlock;

        }
        this.showHoldBlock();
        this.showNextBlocks();
        this.holdUsed = true;
    }

    showHoldBlock(){   
        $("#hold" + this.name).removeClass().addClass("full-block" + this.hold.num);
    }

    //nextBlock functions
    nextBlockEvent(){
        this.block = this.nextBlock1;
        this.nextBlock1 = this.nextBlock2;
        this.nextBlock2 = this.nextBlock3;
        this.nextBlock3 = new Block();
        this.showNextBlocks();
    }
    showNextBlocks(){
        $("#nb1" + this.name).removeClass().addClass("full-block" + this.nextBlock1.num);
        $("#nb2" + this.name).removeClass().addClass("full-block" + this.nextBlock2.num);
        $("#nb3" + this.name).removeClass().addClass("full-block" + this.nextBlock3.num);
    }

    
    //fix block function
    fixBlock(){
        this.holdUsed = false; // reset hold
        this.block.fixed = true;
        let isAttackLineDeleted = false;
        for(var i = 0; i < this.block.shape.length; i++){
            for(var j = 0; j < this.block.shape.length; j++){
                let x = this.block.x + j;
                let y = this.block.y + i;
                if(this.isInsideWall(x) && this.isAboveFloor(y)){
                    if(this.board[y][x] > 0 && this.board[y][x] <= 7 && !this.cache[y][x]){
                        this.cache[y][x] = true;
                    }
                    if(this.isAboveFloor(y + 1)){
                        if(this.block.shape[i][j] !== 0 && this.board[y + 1][x] === 10 && !isAttackLineDeleted){
                             this.block.y++;
                             this.board.splice(y + 1, 1);
                             this.board.unshift(Array(10).fill(0));
                             this.cache.splice(y + 1, 1);
                             this.cache.unshift(Array(10).fill(false));
                             isAttackLineDeleted = true;
                        }
                    }
                }
            }
        }
        this.comboUpdate();
    }
    
    //clear & attack function
    clearLines(){
        let isAnyBlockCleared = false;
        this.board.forEach((row, y) => {
            if(row.every(value => value > 0 && value !== 9)){
                this.block.fixed = true;
                this.block.hardDropped = true;
                this.board.splice(y, 1);
                this.board.unshift(Array(10).fill(0));
                this.cache.splice(y, 1);
                this.cache.unshift(Array(10).fill(false));
                this.clearLinesNum++;
                //this.clearLinesNum = 1;
                this.totalClearLinesNum++;
                this.showTotalClearLines();
                isAnyBlockCleared = true;
            }
        });
        return isAnyBlockCleared;
    }

    showTotalClearLines(){
        var num = (document).getElementById("line-sent-text" + this.name);
        num.innerHTML = this.totalClearLinesNum;
        if(this.totalClearLinesNum < 10){
            $(num).css('color', '#ff7676');
        }
        else if(this.totalClearLinesNum >= 10 && this.totalClearLinesNum < 20){
            $(num).css('color', '#ff9325');
        }
        else if(this.totalClearLinesNum >= 20 && this.totalClearLinesNum < 30){
            $(num).css('color', '#abd500');
        }
        else if(this.totalClearLinesNum >= 30 && this.totalClearLinesNum < 40){
            $(num).css('color', '#6cf2ff');
        }
        else if(this.totalClearLinesNum >= 40 && this.totalClearLinesNum < 50){
            $(num).css('color', '#b609ff');
        }
        else{
            $(num).css('color', '##1bf5ff');      
        }
    }

    attackedLines(num){
        let isBlockSaved = false;
        var b = JSON.parse(JSON.stringify(this.block));
        b.y += 2;
        let attackedNum = 0;

        for(var i = 0; i < num; i++){
            if(!this.isValid(b) && !isBlockSaved){
                while(!this.isValid(b) && !isBlockSaved){
                    if(this.block.y <= 0){
                        this.savedBlock = this.block;
                        isBlockSaved = true;
                        break;
                    }
                    this.block.y--;
                    b.y--;
                }
            }               
      
            this.board.shift();
            this.board.push(Array(10).fill(9));  
            this.cache.shift();
            this.cache.push(Array(10).fill(true));
            var randomBoom = Math.floor(Math.random() * 10);
            //var randomBoom = 5;
            this.board[19][randomBoom] = 10;
            attackedNum++;

            if(this.KOCheck()){
                this.KOEvent();
                if(isBlockSaved){
                    this.block = this.savedBlock;  
                }
                break;
            }        
        }
        return num - attackedNum ;
    }

    clearAttackBar(){
        $("#attack-bar" + this.name).css('height', 0).css('top', 0);
    }

    showAttackBar(num){
        var height = num * 30;
        var top = 700 - height;
        $("#attack-bar" + this.name).css('height', height).css('top', top);
    }
    
    // board functions
    newBoard(){
        this.board = Array.from({length : 20}, () => Array(10).fill(0));
        this.cache = Array.from({length : 20}, () => Array(10).fill(false));
    }

    showBoard(){
        for(var i = 0; i < 20; i++){
            for(var j = 0; j < 10; j++){
                var colorNum = this.board[i][j];
                var curPosToNum = posToNum(j, i);
                $("#" + this.name + curPosToNum).removeClass();
                if(colorNum > 0){                
                    $("#" + this.name + curPosToNum).addClass("block" + colorNum);
                }

           }
       }
    }

    resetBoard(){ 
        for(var i = 0; i < 20; i++){
            for(var j = 0; j < 10; j++){
                if(!this.cache[i][j]){
                    this.board[i][j] = 0;
                }
            }
        }
    }

    setBlockOnBoard(){
        if(!this.block.fixed){
            for(var i = 0; i < this.block.shape.length; i++){
                for(var j = 0; j < this.block.shape.length; j++){
                    if(this.block.shape[i][j] > 0){
                        var x = this.block.x + j;
                        var y = this.block.y + i;
                        var sx = this.block.sx + j;
                        var sy = this.block.sy + i;
                        this.board[sy][sx] = 8;
                        this.board[y][x] = this.block.shape[i][j];
                    }
                }
            }
        }
        var b = JSON.parse(JSON.stringify(this.block));
        b.y++;
        //if(!this.isValid(b)){
           
        //}  
    }

    // valid functions
    isValid(b){
        if(b.hardDropped){
            return false;
        }
        return b.shape.every((row, dy) => {
            return row.every((value, dx) => {
                let x = b.x + dx;
                let y = b.y + dy;
                return (value === 0 || (this.isInsideWall(x) && this.isAboveFloor(y) && this.isNotOcuppied(x, y)));
            })
        })
    }
  
    isInsideWall(x){
        return (x >= 0 && x < 10);
    }
    isAboveFloor(y){
        return (y >= 0  && y < 20); // above floor and below top;
    }
    isNotOcuppied(x, y){
        return !this.cache[y][x];
    }
    
    //shadow function 
    isShadowValid(b){
        return b.shape.every((row, dy) => {
            return row.every((value, dx) => {
                let x = b.sx + dx;
                let y = b.sy + dy;
                return (value === 0 || (this.isInsideWall(x) && this.isAboveFloor(y) && this.isNotOcuppied(x, y)));
            })
        })
    }

    shadowEvent(){
        this.block.sx = this.block.x;
        this.block.sy = this.block.y;
        var b = JSON.parse(JSON.stringify(this.block));
        while(this.isShadowValid(b)){
            this.block.shadowMove(b);
            b.sy++;
        }
    }
    
    //combo function
    comboUpdate(){
        if(this.clearLines()){
            this.combo++;
        }
        else{
            this.combo = 0;
        }
        this.comboEvent(); 
    }

    comboEvent(){
        if(this.combo >= 3 && this.combo < 7){
            this.clearLinesNum += 1;
            this.totalClearLinesNum += 1;
        }
        else if(this.combo >= 7){
            this.clearLinesNum += 2;
            this.totalClearLinesNum += 2;
        }
        this.showTotalClearLines();
    } 

    showComboBar(){
        $("#combo" + this.name).removeClass().addClass("combo-" + this.combo);
        $("#canvas" + this.name).css("border", `5px solid ${this.getColorByCombo()}`);
    }

    getColorByCombo(){
        //canvasA.fillText("+1", 30, "white", 150, 330);
        switch(this.combo){
            case 0 : return "#828282";
            case 1 : return "#828282";
            case 2 : return "#FFC200";
            case 3 : return "#FFA700";
            case 4 : return "#FF8000";
            case 5 : return "#FF5D00";
            case 6 : return "#FF4600";
            case 7 : return "#FF1F00";
            case 8 : return "#FF0000";
            case 9 : return "#CD0000";
            default : return "#CD0000";
        }
        
    }
    
    //backgroundEffect function
    showBackgroundEffect(){
        if(this.combo >= 4){
            this.BEImage.update();
            this.BEImage.draw();
        }
    }
}


//others
function posToNum(x, y){
    return x + (y * 10);
}

function isBoardFull(board){
    for(var i = 0; i < board[0].length; i++){
        if(board[0][i] != 0){
            return true;
        }
    }
    return false;
}


const shapes = {
    one: 
       [[0, 0, 0, 0],
        [1, 1, 1, 1],
        [0, 0, 0, 0],
        [0, 0, 0, 0]],

    two: 
       [[0, 0, 0],
        [0, 2, 2],
        [2, 2, 0]],

    three:
       [[3, 3, 0],
        [0, 3, 3],
        [0, 0, 0]],

    four:
       [[4, 4],
        [4, 4]],

    five:
       [[0, 0, 0],
        [5, 5, 5],
        [5, 0, 0]],

    six:
       [[0, 0, 0],
        [6, 6, 6],
        [0, 6, 0]],
        
    seven:
       [[0, 0, 0],
        [7, 7, 7],
        [0, 0, 7]]
}
Object.freeze(shapes);
