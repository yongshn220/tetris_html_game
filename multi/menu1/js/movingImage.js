class MovingImage{

    constructor(src, canvas, timeLevel){
        this.img = new Image();
        this.img.src = src;
        this.canvas = canvas;
        this.curTime = 0;
        this.maxTime = timeLevel;
    }

    setInformation(sx, sy, sw, sh, dx, dy, dw, dh, maxWidth){
        this.sx = sx;
        this.sy = sy;
        this.sw = sw;
        this.sh = sh;
        this.dx = dx;
        this.dy = dy;
        this.dw = dw;
        this.dh = dh;
        this.maxWidth = maxWidth;
    }

    update(){
        console.log(this.curTime);
        this.curTime++;
        if(this.curTime > this.maxTime){
            this.sx += this.sw;
            if(this.sx >= this.maxWidth){
                this.sx = 0;
            }
            this.curTime = 0;
        }
    }

    draw(){
        this.canvas.context.drawImage(this.img, this.sx, this.sy, this.sw, this.sh, this.dx, this.dy, this.dw, this.dh);
    }
}